<?php 
    if ($projects->state==1) {
        $a=1;
    }
    else{
        $a=2;
    }
    if ($projects->project=="") {
        $c="No hay ningun archivo en este proyecto";
    }
    else{
        $c="Archivo actual: ".$projects->project;
    }
?>
<div class="modal fade" id="modal1{{ $projects->id }}" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Editar proyecto</h4></div>
{!!Form::model($projects, ['route' => 'project/update', 'method' => 'PUT', 'id' => 'formUpdateProject', 'enctype'=>'multipart/form-data'])!!}
    {{ csrf_field() }}
        <div class="form-group">
            <div class="col-md-2">
            <input id="idP" type="hidden" class="form-control" name="idP" value="{{ $projects->id }}" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <label for="email" class="">Nombre del proyecto</label>
                 <input id="NameP" type="text" class="form-control" name="NameP" value="{{ $projects->name_project }}" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <label for="email" class="">Descripcion</label>
                <textarea id="description" type="text" class="form-control" name="description" required>{{ $projects->description }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
            <label for="email" class="">Estado</label>
        <select class="form-control" id="states" name="states">
                <option value="<?php echo($a); ?>" selected>
                    @if($a==1)
                        Activo
                    @else
                        Inactivo    
                    @endif
                </option>
                <option value="<?php if($a==1){echo(2);}else{echo(1);}?>">
                    <?php if($a==1){echo('Inactivo');}
                    else{echo "Activo";}?>
                </option>
        </select>  
    </div>
    </div>
    <div class="form-group">
            <div class="col-md-12">
                <label for="email" class="">Imagen</label>
                <input type="text" class="form-control" value="{{ $projects->image }}" name="imageF" readonly="readonly">
                <input id="image" type="file" class="form-control" name="image">
            </div>
    </div>
    <div class="form-group">
            <div class="col-md-12">
                <label for="email" class="">Archivo</label>
                <input type="text" class="form-control" value="<?php echo "$c"; ?>" readonly="readonly">
                <input id="Pproject" type="file" class="form-control" name="Pproject">
            </div>
    </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-success">
                    Actualizar proyecto
                </button>
            </div>
        </div>  
        {!! Form::close() !!}
</div>
</div>
</div>