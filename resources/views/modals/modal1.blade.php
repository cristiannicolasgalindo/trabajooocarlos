<div class="modal fade" id="modalregistrar" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Crear proyecto</h4></div>
{!! Form::open(['route' => 'project.store', 'method' => 'post', 'id'=>'formNewProject', 'enctype'=>'multipart/form-data']) !!}
    {{ csrf_field() }}
        <div class="form-group">
            <div class="col-md-2">
            <input id="idP" type="hidden" class="form-control" name="idP" value="" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <label for="email" class="">Nombre del proyecto</label>
                 <input id="NameP" type="text" class="form-control" name="NameP" value="" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <label for="email" class="">Descripcion</label>
                <textarea id="description" type="text" class="form-control" name="description" required></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
            <label for="email" class="">Estado</label>
        <select class="form-control" id="states" name="states">
                <option value="1" selected>
                        Activo 
                </option>
                <option value="2">
                    Inactivo
                </option>
        </select>  
    </div>
    </div>
        <div class="form-group">
            <div class="col-md-12">
                <label for="email" class="">Imagen</label>
                <input id="image" type="file" class="form-control" name="image">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <label for="email" class="">Archivo</label>
                <input id="Pproject" type="file" class="form-control" name="Pproject">
            </div>
        </div>
        @if(count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Ocurrio un error :(</strong> Revisa que todos los campos esten diligenciados y que la imagen sea de tipo: jpeg, png o jpg.
        </div>
        @endif
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-success">
                    Registrar proyecto
                </button>
            </div>
        </div>  
        {!! Form::close() !!}
</div>
</div>
</div>