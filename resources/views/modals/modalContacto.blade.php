<div class="modal fade" id="modalC" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Contacto</h4></div>
{!! Form::open(['route' => 'contact.store', 'method' => 'post', 'id'=>'formContact']) !!}

        <div class="form-group">
            <div class="col-md-12">
                <label for="email" class="">E-mail</label>
                 <input id="from" type="email" class="form-control" name="from" value="" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <label for="email" class="">Asunto</label>
                <input id="affair" type="text" class="form-control" name="affair" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <label for="email" class="">Mensaje</label>
                <textarea id="message" type="text" class="form-control" name="message" required></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-success">
                    Enviar
                </button>
            </div>
        </div>  
        {!! Form::close() !!}
</div>
</div>
</div>