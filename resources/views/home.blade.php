@extends('layouts.app')
<br><br><br><br><br><br>
@section('content')
<style type="text/css">
.imag{border: 11px solid black;}
</style>
    <section class="bg-primary" id="portafolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">GESTIONAR PORTAFOLIO</h2><br><br>

    <section class="p-0" id="portfolio">
      <div class="container-fluid">
        <div class="row no-gutter popup-gallery">

          @foreach($projects as $projects)
              <div class="col-lg-4 col-sm-6 imag">
                <div class="portfolio-box">
                  <img class="img-fluid" src="img/portfolio/thumbnails/{{$projects->image}}" alt="">
                  <div class="portfolio-box-caption">
                    <div class="portfolio-box-caption-content">
                      <div class="project-category text-faded">
                        {{ $projects->name_project }}
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-success" data-toggle="modal" data-target="#modal1{{ $projects->id }}">
                                Editar
                            </button>
                          </div>
                          <div class="col-md-6 col-md-offset-4">
                          <a class="btn btn-danger" href="{{ route('project.destroy', $projects->id) }}">Eliminar</a>
                          </div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
              
@include('modals.modalPortafolio')
            @endforeach
              <div class="col-lg-4 col-sm-6 imag">
                <a class="portfolio-box" data-toggle="modal" data-target="#modalregistrar">
                  <img class="img-fluid" src="img/portfolio/thumbnails/7.png" alt="">
                  <div class="portfolio-box-caption">
                    <div class="portfolio-box-caption-content">
                      <div class="project-category text-faded">
                        Proyecto
                      </div>
                      <div class="project-name">
                        Agregar +
                      </div>
                    </div>
                  </div>
                </a>
              </div>

              </div>
              </div>
        </section>
          </div>
        </div>
      </div>
    </section>
@include('modals.modal1')

@endsection
