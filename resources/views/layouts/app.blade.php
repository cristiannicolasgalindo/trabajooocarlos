<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">



    <title>Blog Cristian</title>

    <!-- Bootstrap core CSS -->


     <link rel="stylesheet" href="{{ asset('css/bootstrap/css/bootstrap.css') }}">

    <!-- Custom fonts for this template -->


      <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" type="text/css">


    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->

     <link rel="stylesheet" href="{{ asset('css/magnific-popup/magnific-popup.css') }}">

    <!-- Custom styles for this template -->

    <link rel="stylesheet" href="{{ asset('css/css/creative.min.css') }}">

    <style type="text/css">
        body{
        background-image: url('/img/header.jpg');
        background-repeat: no-repeat;
        background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
      background-position: center;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
        }
    </style>

  </head>

  <body id="page-top">

      
  
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
       @if(\Auth::user())
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}">Vista previa</a>
       @else
        <p class="navbar-brand js-scroll-trigger">¡ Registrate para conocer mejor el sistema !</p>
       @endif
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            @if(\Auth::user())
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Mas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Sobre mi</a>
            </li>
            @else
              <a class="nav-link js-scroll-trigger" href="{{ url('/') }}">Volver a pagina principal</a>
            @endif
              
            </li>
            @if(\Auth::user())
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portafolio"> Gestionar portafolio</a>
            </li>
              <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="{{ url('/logout') }}">Cerrar sesion</a>
              </li>
              @endif
          </ul>
        </div>
      </div>
    </nav>
  
   

    <!-- Bootstrap core JavaScript -->

    <script type="text/javascript" src="{{ asset('js/jquery/jquery.min.js') }}"></script>

     <script type="text/javascript" src="{{ asset('js/popper/popper.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Plugin JavaScript -->

    <script type="text/javascript" src="{{ asset('js/jquery-easing/jquery.easing.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/scrollreveal/scrollreveal.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/magnific-popup/jquery.magnific-popup.min.js') }}"></script>

    <!-- Custom scripts for this template -->

    <script type="text/javascript" src="{{ asset('js/js/creative.min.js') }}"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    @yield('content')

  </body>

</html>


