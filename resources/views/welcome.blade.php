<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">



    <title>Blog Cristian</title>

    <!-- Bootstrap core CSS -->


     <link rel="stylesheet" href="{{ asset('css/bootstrap/css/bootstrap.css') }}">

    <!-- Custom fonts for this template -->


      <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" type="text/css">


    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->

     <link rel="stylesheet" href="{{ asset('css/magnific-popup/magnific-popup.css') }}">

    <!-- Custom styles for this template -->

    <link rel="stylesheet" href="{{ asset('css/css/creative.min.css') }}">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
       @if (Auth::guest())
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Blog de Cristhian</a>
      @else
        @if(\Auth::user()->rol_id==1)
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/home') }}">Volver a administracion</a>
        @else
        <a class="navbar-brand js-scroll-trigger" href="#page-top">{{ Auth::user()->name }}</a>
        @endif
      @endif
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Mas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Sobre mi</a>
            </li>
             @if (Auth::guest())
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" data-toggle="modal" data-target="#myModal" href="">Iniciar sesion</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ url('/register') }}">Registrarse</a>
              </li>
             @endif
            @if(\Auth::user())
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Portafolio</a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contacto</a>
            </li>
            @if(\Auth::user())
              <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="{{ url('/logout') }}">Cerrar sesion</a>
              </li>
            @endif
          </ul>
        </div>
      </div>
    </nav>
  
    <header class="masthead">
      <div class="header-content">
        <div class="header-content-inner">
          <h1 id="homeHeading">Cristhian Nicholas Galindo Gutierrez</h1>
          <hr>
          <p> Registrate para conocer los proyectos </p>
          <a class="btn btn-success btn-xl js-scroll-trigger" href="#about">Mas información</a>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">¡Tenemos lo que necesitas!</h2>
            <hr class="light">
            <p class="text-faded">En este blog podras explorar todos y cada uno de los proyectos existentes, realizados por mi a lo largo de mi carrera como desarrollador de software. Estos proyectos los podras descargar una vez te registres en este sistema de informacion.</p>
            <a class="btn btn-success btn-xl js-scroll-trigger" href="#services">Ver mas</a>
          </div>
        </div>
      </div>
    </section>

    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Analisis y desarrollo de sistemas de informacion</h2>
            <hr class="primary">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box">
              <i class="fa fa-4x fa-diamond text-primary sr-icons"></i>
              <h3>Base de datos</h3>
              <p class="text-muted">Entre las bases de datos en las que desarrollo estan: MYSQL, POSTGRESQL, SQL y ORACLE</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box">
              <i class="fa fa-4x fa-diamond text-primary sr-icons"></i>
              <h3>Java</h3>
              <p class="text-muted">Podras encontrar aplicaciones y softwares en JAVA</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box">
              <i class="fa fa-4x fa-diamond text-primary sr-icons"></i>
              <h3>PHP</h3>
              <p class="text-muted">En este lenguaje de programacion se desarrollan la mayoria de los proyectos realizados por mi, en laravel y PHP limpio</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box">
              <i class="fa fa-4x fa-diamond text-primary sr-icons"></i>
              <h3>English</h3>
              <p class="text-muted">La mayoria de los proyectos (por no decir todos) se encuentran en inglés, debido al profesionalismo y entendimiento que genera</p>
            </div>
          </div>
        </div>
        @if(\Auth::user())
        <br><br><br>
        <center><a class="btn btn-success btn-xl js-scroll-trigger" href="#portfolio">Portafolio</a></center>
        @endif
      </div>
    </section>
@if(\Auth::user())
<section class="p-0" id="portfolio">
    <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Portafolio</h2>
            <hr class="primary">
          </div>
        </div>
      </div>
      <div class="col-lg-12 col-md-6 text-center">
            <div class="service-box">
              <p class="text-muted">Hola {{\Auth::user()->name }} esta seccion puedes dar click en los proyectos disponibles y para leer su descripcion y luego descargarlo si deseas</p>
            </div>
          </div><br><br>
@foreach($projects as $projects)
@if($projects->state==1)
 <div class="column-sm-4">
      <div class="container-fluid">
            <a class="portfolio-box" href="">
              <img class="img-fluid" src="images/{{$projects->image}}" style="width: 497px; height: 390px;">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-name" style="word-wrap: break-word;">
                    {{$projects->name_project}}
                  </div>
                  <div class="project-category text-faded" style="word-wrap: break-word;">
                    {{$projects->description}}
                  </div>
                </div>
              </div>
            </a>
      </div>
    </section>
  </div><br>
@endif
@endforeach
<br><br>
@endif
    <div class="call-to-action bg-dark" id="contact">
      <div class="container text-center">
        <h2>Contactate conmigo</h2>
        <a class="btn btn-success btn-xl sr-button" data-toggle="modal" data-target="#modalC">Contactarse</a>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->

    <script type="text/javascript" src="{{ asset('js/jquery/jquery.min.js') }}"></script>

     <script type="text/javascript" src="{{ asset('js/popper/popper.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Plugin JavaScript -->

    <script type="text/javascript" src="{{ asset('js/jquery-easing/jquery.easing.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/scrollreveal/scrollreveal.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/magnific-popup/jquery.magnific-popup.min.js') }}"></script>

    <!-- Custom scripts for this template -->

    <script type="text/javascript" src="{{ asset('js/js/creative.min.js') }}"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


   @include('auth.login')
   @include('modals.modalContacto')

  </body>

</html>
