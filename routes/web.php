<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;

Route::get('/', function () {
    return redirect()->action('ProjectController@index');
});

Auth::routes();

Route::get('/login', function(){
	return view('auth/login');
});

Route::get('/logout', function(){
	Auth::logout();
	return redirect('/');
});

Route::get('/home', 'HomeController@index');

Route::resource('project','ProjectController');
Route::post('project/update', ['as'=>'project/update', 'uses'=>'ProjectController@update']);
Route::post('project/store', ['as'=>'project/store', 'uses'=>'ProjectController@store']);
Route::get('project.destroy/{id}', ['as'=>'project.destroy', 'uses'=>'ProjectController@destroy']);

Route::resource('contact','ContactController');
Route::post('contact/store', ['as'=>'contact/store', 'uses'=>'ContactController@store']);

Route::get('/sendemail', function(Request $request){

	Mail::send('modals.modalContacto',[], function(Message $message){
		$message->to('crisgalindo22@gmail.com')
		->from('jdgalindo05@gmail.com')
		->subject('esto no funciona');
	});

})->name('sendemail');
