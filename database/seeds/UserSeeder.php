<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $users = array(
        [
          'name' => 'Cristian Galindo',
          'email' => 'cngalindo@misena.edu.co',
          'password' => bcrypt('1234567'),
          'rol_id' => 1,
        ],
        [
          'name' => 'Pedro',
          'email' => 'pedrito@gmail.com',
          'password' => bcrypt('1234567'),
          'rol_id' => 2,
        ]
      );

      foreach ($users as $value)
      {
        $user = new User;
        $user->name = $value['name'];
        $user->email = $value['email'];
        $user->password = $value['password'];
        $user->rol_id = $value['rol_id'];
        $user->save();
      }
    }
}
