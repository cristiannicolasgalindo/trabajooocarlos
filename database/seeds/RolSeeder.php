<?php

use Illuminate\Database\Seeder;
use App\Models\Rol;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $roles = array(
        [
          'name_role' => 'Administrador',
        ],
        [
          'name_role' => 'Otro',
        ]
      );

      foreach ($roles as $value)
      {
        $rol = new Rol;
        $rol->name_role = $value['name_role'];
        $rol->save();
      }
    }
}
