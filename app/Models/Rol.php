<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Rol extends Model
{
    protected $table = 'rol';
    protected $fillable = ['name_role'];
    protected $guarded = ['id'];
}