<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Project extends Model
{
    protected $table = 'project';
    protected $fillable = ['name_project','project','state','description','image'];
    protected $guarded = ['id'];
}