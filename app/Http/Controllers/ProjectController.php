<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project as Project;


class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects= Project::all();
        return \View::make('welcome',compact('projects'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $project= new Project();
            $project->name_project = $request->NameP;
            $project->project = $request->Pproject;
            $project->state = $request->states;
            $project->description = $request->description;

            $this->validate($request, ['image'=> 'required|image|mimes:jpeg,png,jpg|max:2048',]);

            $nameI= time().'.'.$request->image->getClientOriginalExtension();

            $request->image->move(public_path('images'), $nameI);

            $project->image=$nameI;
            $project->save();

        return back()->with('success', 'El proyecto se ha resgistrado correctamente')->with('path',$nameI);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);
        return \View::make('modals/modalPortafolio',compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

            $project= Project::find($request->idP);

            $project->name_project = $request->NameP;
            $project->project = $request->Pproject;
            $project->state = $request->states;
            $project->description = $request->description;

            if ($request->image==null) {
                $project->image=$request->imageF;
            }
            else{

                $this->validate($request, ['image'=> 'required|image|mimes:jpeg,png,jpg|max:2048',]);
                $nameI= time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('images'), $nameI);
                $project->image=$nameI;
            }
            $project->save();

        return redirect()->action('HomeController@index');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $project->delete();
        return redirect()->action('HomeController@index');
    }
}
