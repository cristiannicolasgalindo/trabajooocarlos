<?php

namespace App\Http\Controllers;
use App\Models\Project;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects= Project::all();
        if (\Auth::user()->rol_id==2) {
             return \View::make('welcome',compact('projects'));
        }
        else{
            return \View::make('home',compact('projects'));
        }
        
    }
}
